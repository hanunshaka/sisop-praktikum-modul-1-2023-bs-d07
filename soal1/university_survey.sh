#!/bin/bash

# Get 5 highest ranked universities in Japan
data=$(cat '2023 QS World University Rankings.csv')
japan_data=$(echo "$data" | awk -F',' '$4 == "Japan"' | head -n 5)

echo "Best 5 Universities in Japan:"
echo "$japan_data" | awk -F',' '{print $2}' | cat -n
echo " "

# Get university with lowest fsr score amongst the 5 universities
lowest_fsr=$(echo "$japan_data" | awk -F',' '{print $9}' | sort -n | head -n 1)
lowest_fsr_univ=$(echo "$japan_data" | awk -F',' -v lowest_fsr="$lowest_fsr" '$9 == lowest_fsr {print $2}')
echo "Lowest fsr score amongst the 5 universities:"
echo "$lowest_fsr_univ ($lowest_fsr)"
echo ""

# REVISI
# Get 5 universities in Japan with loweest fsr score
echo "5 Universities in Japan with lowest fsr score:"
echo "$data" | awk -F',' '$4 == "Japan"' | sort -t',' -k9 -n | head -n 5 | awk -F',' '{print $2 " ("$9")"}' | cat -n
echo ""

# Get university with highest ger ranking in Japan
highest_ger=$(echo "$data" | awk -F',' '{if ($4 == "Japan"){print $20}}' | sort -n | head -n 1)
highest_ger_univ=$(echo "$data" | awk -F',' -v highest_ger="$highest_ger" '$20 == highest_ger {print $2}')
echo "Highest ger in Japan:"
echo "$highest_ger_univ $highest_ger"
echo ""

#REVISI
# Get university with most ger score ranking in Japan
highest_ger_score=$(echo "$japan_data" | awk -F',' '{if ($4 == "Japan"){print $19}}' | sort -n -r | head -n 1)
highest_ger_univ=$(echo "$japan_data" | awk -F',' -v highest_ger="$highest_ger_score" '$19 == highest_ger {print $2}')
echo "Highest ger score in Japan:"
echo "$highest_ger_univ $highest_ger_score"
echo ""

# Get university with keyword 'keren'
echo "University with keyword 'keren':"
echo "$data" | awk -F',' '$2 ~ /Keren/ {print $2}'
