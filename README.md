# Lapres Praktikum Modul 1 Kelompok D07
Anggota Kelompok ''D07'' 
| Nama                      | NRP        |
|---------------------------|------------|
| Hanun Shaka Puspa         | 5025211051 |
| Mavaldi Rizqy Hazdi       | 5025211086 |
| Daffa Saskara             | 5025201249 |

## 1. University Survey
Semua pertanyaan di soal nomor ini dijawab menggunakan awk dalam file shell university_survey.sh.

### Penjelasan Solusi
- Pertanyaan 1
	Untuk mendapatkan 5 universitas terbaik di Jepang, cukup mengambil row dengan value kolom ke-4 "Japan" dengan awk lalu dibatasi dengan "head -n 5". Tidak perlu diurutkan lagi karena ranking dari data csv sudah terurut.
- Pertanyaan 2
	Untuk mendapatkan 5 universitas di Jepang dengan skor fsr terendah, caranya cukup mirip dengan pertanyaan 1. Dilakukan sorting dengan kode ```sort -t',' -k9 -n```. Awk akan mengurutkan sesuai value terendah di kolom 9, yaitu kolom skor fsr.
- Pertanyaan 3
	Untuk mendapatkan universitas di Jepang dengan skor ger tertinggi, dicari berapa nilai ger tertinggi dengan awk. Mengecek lokasi di "Japan", kemudian melakukan sorting reversed karena diminta nilai terbesar, dan disimpan nilai dari row pertama yang muncul dengan kode ```sort -n -r | head -n 1```
	Lalu dilakukan pencarian row yang memiliki nilai kolom ke-19 sama dengan hasil pencarian skor ger yang telah dilakukan. Kemudian dicetak nama universitas dan skor gernya.
- Pertanyaan 4
	Untuk mendapatkan universitas dengan keyword 'keren', dapat dilakukan dengan awk sederhana ```echo "$data" | awk -F',' '$2 ~ /Keren/ {print $2}'```.

### Screenshot Solusi University Survey
![University Survey](https://i.ibb.co/52fVdB5/Whats-App-Image-2023-03-10-at-16-01-39.jpg)

### Kendala Pengerjaan Soal
- Untuk pertanyaan kedua
	```
	Karena Bocchi kurang percaya diri dengan
	kemampuannya, coba cari Faculty Student Score(fsr score)
	yang paling rendah diantara 5 Universitas di Jepang. 
	```
	Saya rasa dari soal di atas, yang diminta adalah universitas dengan skor fsr terendah dari 5 universitas dari Jepang yang sudah didapatkan dari pertanyaan pertama. Namun saat demo, saya diminta untuk merevisi jawaban sehingga didapat 5 universitas di Jepang dengan skor fsr terendah. Untuk revisinya sudah saya sertakan di solusi soal 1.
- Untuk pertanyaan ketiga
	```
	Karena Bocchi takut salah membaca ketika memasukkan
	nama universitas, cari 10 Universitas di Jepang dengan
	Employment Outcome Rank(ger rank) paling tinggi.
	```
	Seperti pertanyaan kedua, saya rasa yang diminta di soal adalah ranking ger tertinggi, tapi ternyata saat demo dijelaskan bahwa yang diminta adalah skor ger tertinggi. Sudah saya sertakan revisinya di solusi soal 1.


## 2. Kobeni Liburan
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.
### Soal A
Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
-File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
-File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) 
**Penjelasan solusi :**
```sh
exact_hour=$(date +%H)
download_num=$((exact_hour))

if [ $exact_hour -eq 0 ]; then
download_num=1
else
download_num=$exact_hour
fi
```
- Membuat variable ``exact_hour`` yang dapat menangkap jam saat ini dengan fungsi ``(date + "%H")``. 
- Lalu, nilai tersebut distore pada variabel ``download_num``. Lalu masuk ke if untuk memenuhi persyaratan saat di jam 00, maka akan tetap men-download 1 gambar.
```sh
nama_folder="kumpulan_$(ls | grep -c '^kumpulan_[0-9]*$' | awk '{print $1+1}')"
mkdir $nama_folder
```
Script tersebut bertujuan untuk membuat direktori baru dengan nama yang mengikuti pola "kumpulan_" diikuti oleh sebuah angka yang bertambah setiap kali skrip dijalankan.
- ``ls`` untuk melihat daftar folder dan file yang ada pada directory tersebut.``grep -c`` untuk menghitung jumlah direktori yang sudah ada dengan pola nama ``kumpulan_`` di awal, diikuti oleh angka.
- Kemudian, ``awk '{print $1+1}'`` digunakan untuk menambahkan nilai 1 pada jumlah yang dihitung sebelumnya, sehingga akan menghasilkan nomor urut yang baru.
- Variabel ``nama_folder`` didefinisikan dengan menyimpan string ``kumpulan_`` diikuti oleh nomor urut yang baru saja dihasilkan.
```sh
for (( i=1; i<=$exact_hour; i=i+1 ));
do
  wget "https://source.unsplash.com/600x400/?wayang" -O "${nama_folder}/perjalanan_${i}.FILE"
done
}
```
- loop ``for`` yang akan mengeksekusi perintah ``wget`` berulang-ulang sebanyak ``$exact_hour`` kali.
- Pada setiap iterasi, perintah wget akan mendownload sebuah gambar dari URL ``https://source.unsplash.com/600x400/?wayang`` dan menyimpannya dengan nama file ``perjalanan_`` diikuti dengan nomor urut iterasi yang sedang berlangsung, serta ditambahkan ekstensi file ``.FILE``.
- Variabel ``$nama_folder`` yang telah didefinisikan sebelumnya akan digunakan sebagai direktori tempat menyimpan file hasil download.
- Baris kedua menggunakan perintah ``mkdir`` untuk membuat direktori baru dengan nama yang disimpan dalam variabel ``nama_folder.``
```sh
case_folder()
{
exact_hour=$(date +%H)
download_num=$((exact_hour))

if [ $exact_hour -eq 0 ]; then
download_num=1
else
download_num=$exact_hour
fi


nama_folder="kumpulan_$(ls | grep -c '^kumpulan_[0-9]*$' | awk '{print $1+1}')"
mkdir $nama_folder


for (( i=1; i<=$exact_hour; i=i+1 ));
do
  wget "https://source.unsplash.com/600x400/?wayang" -O "${nama_folder}/perjalanan_${i}.FILE"
done
}
```
Di rangkum menjadi fungsi/case ``case_folder()`` dan dijalankan oleh cronjob selama 10 jam sekali dengan syntax berikut :
```sh
* */10 * * * /bin/bash /mnt/c/Users/daffa/OneDrive/Documents/DAFFA'S/tugsadas/SISOP/prak1no2/kobeni_liburan.sh case_folder
```
### Soal B
Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas. 
**Penjelasan solusi :**
```sh
zip_counter=$(ls | grep -c '^devil_[0-9]*$' | awk '{print $1+1}')
```
- ``grep -c`` untuk menghitung jumlah file yang sudah ada dengan pola nama ``devil_`` di awal, diikuti oleh sebuah angka. Kemudian, ``awk '{print $1+1}'`` digunakan untuk menambahkan nilai 1 pada jumlah yang dihitung sebelumnya, sehingga akan menghasilkan nomor urut yang baru.
```sh
folder_counter=$(ls | grep -c '^kumpulan_[0-9]*$' | awk '{print $1}')
```
- Baris variabel ``folder_counter`` melakukan hal yang sama dengan baris sebelumnya ( ``zip_counter`` ), namun untuk menghitung jumlah direktori yang sudah ada dengan pola nama ``kumpulan_`` di awal, diikuti oleh sebuah angka. Kemudian, nilai tersebut disimpan dalam variabel ``folder_counter``.
```sh
zip_name="/mnt/c/Users/daffa/OneDrive/Documents/DAFFA'S/tugsadas/SISOP/prak1no2/devil_${zip_counter}"
```
- Variabel ``zip_name`` didefinisikan dengan menyimpan string ``"/mnt/c/Users/daffa/OneDrive/Documents/DAFFA'S/tugsadas/SISOP/prak1no2/devil_"`` (menggunakan petik 2 pada dir karena ada file yang menggunakan symbol petik (DAFFA'S) ), diikuti oleh nomor urut yang baru saja dihasilkan.
Secara keseluruhan, skrip tersebut digunakan untuk menentukan nama dan nomor urut untuk file zip yang akan dibuat nantinya.
```sh
case_zip()
{
zip_counter=$(ls | grep -c '^devil_[0-9]*$' | awk '{print $1+1}')
folder_counter=$(ls | grep -c '^kumpulan_[0-9]*$' | awk '{print $1}')
zip_name="/mnt/c/Users/daffa/OneDrive/Documents/DAFFA'S/tugsadas/SISOP/prak1no2/devil_${zip_counter}"
	
for (( i=1; i <= folder_counter; i=i+1 )) 
do

	zip -r $zip_name.zip kumpulan_$i
  rm -r "/mnt/c/Users/daffa/OneDrive/Documents/DAFFA'S/tugsadas/SISOP/prak1no2/kumpulan_$i"

done
}
```
Di rangkum menjadi fungsi/case ``case_zip()`` dan dijalankan oleh cronjob selama 24 jam sekali dengan syntax berikut :
```sh
0 */24 * * * /bin/bash /mnt/c/Users/daffa/OneDrive/Documents/DAFFA'S/tugsadas/SISOP/prak1no2/kobeni_liburan.sh case_zip
```
### IF ELSE Pengatur Fungsi / Case
```sh
if [ "$1" = "case_folder" ]; then
  case_folder
elif [ "$1" = "case_zip" ]; then
  case_zip
fi
```
Script tersebut merupakan sebuah blok kondisional ``if-else`` yang digunakan untuk mengecek nilai argumen yang diberikan saat menjalankan script.
- Jika nilai argumen pertama sama dengan string ``case_folder``, maka fungsi ``case_folder`` akan dijalankan.
- Namun, jika nilai argumen pertama sama dengan string ``case_zip``, maka fungsi ``case_zip`` akan dijalankan.
- Jika nilai argumen tidak sama dengan keduanya, maka tidak ada aksi yang diambil. 
### Screenshoot Solusi Soal 2
| <p align="center"> kobeni_liburan.sh case_folder </p> | <p align="center"> kobeni_liburan.sh case_zip </p> |
| -------------------------------------- | -------------------------------------- |
| <img src="/uploads/893d4c6c9032b874004c098fc4c5ea8e/case_folder.jpg" width = "500"/> | <img src="/uploads/8e2dd74750024bf4a4d5d6078c83daf0/case_zip.jpg" width = "500"/> |
### Kendala Pengerjaan Soal 2
Kendal yang ada mungkin adalah membuat penamaan zip agar memiliki angka yang incremental. Dan juga pengaturan Crontab yang sedikit trick.

## 3. Peter Griffin
### Penjelasan Solusi
- Untuk shell script register louis.sh
	Pertama-tama, dicek apakah direktori users sudah ada. Jika belum, dibuat folder baru dengan ```mkdir users``` lalu ```cd users```. Jika sudah ada, cukup lakukan ```cd users```. Digunakan variabel boolean yaitu ```min_length, name_pass_same, use_chicken, use_ernie, not_alnum, not_pass_check``` semuanya diset true di awal script. Variabel tersebut digunakan untuk melakukan while loop saat meminta username dan password dari user. Selama password yang diberikan tidak lolos persyaratan password, program akan terus berulang. Jika password telah lolos pengecekan syarat-syarat password, variabel-variabel tersebut akan diset menjadi false sehingga keluar dari while loop. Kemudian digunakan command ```grep``` untuk mengecek, apakah username sudah ada di file ```users.txt```. Jika sudah ada, throw pesan error ke terminal dan ke dalam file log.txt menggunakan command ```echo```. Jika belum terdaftar, tambahkan username dan password baru dengan delimiter spasi ke line baru di dalam file ```users.txt``` dan simpan pesan login di line baru dalam file ```log.txt``` dengan command ```echo```. 
- Untuk shell script login retep.sh
	Pertama-tama, dicek apakah direktori users sudah ada. Jika belum ada, langsung throw pesan belum ada user yang terdaftar. Jika sudah ada, ```cd users```. Kemudian meminta user untuk menginput username dan password. Digunakan command ```grep``` untuk mengambil row dengan username sesuai input dari file ```users.txt``` lalu di-tokenize username dan passwordnya menggunakan ```awk```. Jika tidak ada, maka hasil ```grep``` akan menjadi kosong. Throw pesan bahwa user tidak terdaftar dan exit program. Jika ditemukan username dalam file ```users.txt```, cek apakah password yang diinput sama dengan password di file ```users.txt```. Jika password berbeda, throw pesan error dan catat lognya di file ```log.txt```. Jika password cocok, ```echo``` pesan login dan catat loginnya di file ```log.txt```.

### Screenshoot Solusi Soal 3
- louis.sh dan retep.sh

![Ss louis.sh dan retep.sh](https://i.ibb.co/yBYVQjZ/Whats-App-Image-2023-03-10-at-16-03-56.jpg)
- users.txt

![Ss isi users,txt](https://i.ibb.co/0jqXPJ7/Whats-App-Image-2023-03-10-at-16-04-24.jpg)
- log.txt

![Ss isi file log](https://i.ibb.co/p11hFrr/Whats-App-Image-2023-03-10-at-16-04-39.jpg)

### Kendala Pengerjaan Soal 3
	Tidak ada kendala yang berarti di pengerjaan solusi soal ketiga

## 4. Johan Liebert
### Penjelasan Solusi
### Enkripsi ```log_encrypt.sh```
Untuk penyelesaian no 4, pertama tentukan format nama file dengan format "jam:menit tanggal:bulan:tahun.txt". Cara mendapatkan format nama file dalam bentuk waktu tersebut dapat menggunakan cara berikut :
```sh
filename="$(date +'%H:%M_%d:%m:%Y').txt"
```
Variabel filename yang nantinya akan digunakan untuk menamai file backup syslog yang telah dienkripsi :
```sh
- command ```date``` digunakan untuk mengambil informasi waktu saat dijalankan
- %H : hours
- %M : minutes
- %d : date/day of month
- %m : month
- %Y : year
```
Enkripsi isi file menggunakan sistem caesar cipher yang mana huruf asli akan diganti dengan huruf lain dengan cara menambahkan nilai yang diinginkan ke nilai huruf asli. Dengan contoh huruf a adalah huruf 1 dan akan digeser sebanyak 2 nilai yang berarti huruf a yang sekarang akan diganti menjadi huruf c yang bernilai 1+2=3. 

Pada soal, pergeseran yang diinginkan harus sesuai dengan waktu dibuatnya file backup syslog yang mana jika file dibuat pukul 10 maka huruf-huruf dalam file backup syslog tersebut akan bergeser sebanyak 10 nilai. Setelah semua isi file syslog dienkripsi, hasilnya akan dioutput ke dalam bentuk file dengan format ```.txt``` dan dengan format nama ```jam:menit_tanggal:bulan:tahun``` yang sesuai dengan waktu file backup tersebut dibuat.

### Dekripsi ```log_decrypt.sh```
Untuk mendekripsi file backup syslog tadinya, kita akan membutuhkan nilai jam atau jam saat file tersebut dienkripsi agar kita dapat melakukan dekripsi dengan metode caesar cipher tadinya. 

Untuk mendapatkan nilai jam dari file tersebut, cara yang pertama tidak bisa lagi digunakan yaitu menggunakan fungsi ```date```. Cara untuk mendapatkan nilai jamnya adalah dengan mengambil angka pertama dari nama file backup syslog yang telah terenkripsi karena nilai jam terdapat pada angka pertama dalam nama file sesuai format. Caranya adalah dengan line berikut :
```sh
hour=$(echo "$encrypted_file" | cut -d'_' -f1 | cut -d':' -f1)
```
Line tersebut nantinya akan mengambil jam enkripsi dari nama file dengan cara memisahkan nama file yang terletak sebelum tanda ```_``` pertama setelah itu bagian yang terpisah tadi diambil karakter sebelum tanda ```:``` pertama sehingga didapatkan nilai jam dari file backup tersebut.

Setelah mendapatkan nilai jam dari file yang akan didekripsi maka dekripsi pada semua isi file backup syslog dapat dilakukan dengan cara dekripsi caesar cipher karena pada saat enkripsi digunakan enkripsi caesar cipher. 

Setelah semua isi file backup syslog terdekripsi, maka hasilnya akan dioutput kedalam bentuk file ```.txt``` dengan format ```decrypted_jam:menit_tanggal:bulan:tahun``` format waktu tersebut sesuai dengan waktu / nama file backup syslog dienkripsi.

### Screenshoot Solusi Soal 4
- Backup syslog encrypted

![isi file backup syslog yang terenkripsi](https://github.com/mavaldi/image-placeeee/blob/main/image_2023-03-10_19-56-38.png?raw=true)
- Backup syslog decrypted

![isi file backup syslog yang terdekripsi](https://github.com/mavaldi/image-placeeee/blob/main/photo_2023-03-10_20-23-19.jpg?raw=true)

### Kendala Pengerjaan Soal 4
Dalam pengerjaan tidak ada kendala, tetapi saat menjalankan enkripsi backup file syslog memakan waktu yang sangat lama > 10 menit.