#!/bin/bash

min_length=true
name_pass_same=true
use_chicken=true
use_ernie=true
not_alnum=true
not_pass_check=true

# Create users directory and users.txt if not exists
if [ ! -d "users" ]
then
  mkdir users
  cd users
  touch users.txt
else
  cd users
fi

# Check credentials rules
while [ $not_pass_check = true ]
do
  # Get credentials
  echo "Enter new username:"
  read user_name
  echo "Enter new password:"
  read user_password

  # Minimal 8 characters
  if [ ${#user_password} -lt 8 ]
  then
    echo "Need to be at least 8 characters"
  else
    min_length=false
  fi

  # Password != username
  if [ "$user_name" == "$user_password" ]
  then
    echo "Password should be different from username"
  else
    name_pass_same=false
  fi

  # Using chicken
  if [ "$user_password" = "chicken" ]
  then
    echo "Musn't use chicken"
  else
    use_chicken=false
  fi

  # Using ernie
  if [ "$user_password" = "ernie" ]
  then
    echo "Musn't use ernie"
  else
    use_ernie=false
  fi

  # Contain at least 1 capital, 1 number
  if echo "$user_password" | grep -qE '[0-9]' && echo "$user_password" | grep -qE '[a-z]' && echo "$user_password" | grep -qE '[A-Z]'; then
    not_alnum=false
  else
    echo "Must contain at least 1 number, 1 capital letter, and 1 lowercase letter"
  fi

  if [ $min_length = true ] || [ $name_pass_same = true ] || [ $use_chicken = true ] || [ $use_ernie = true ] || [ $not_alnum = true ]; then
    not_pass_check=true
  else
    not_pass_check=false
  fi
done

# Get username match from users.txt
temp=$(grep -o -m 1 "$user_name" users.txt)

# Throw if user already exists and add user if not
if [ ${#temp} -gt 0 ]
then
  echo "Username already exists"

  # log it
  echo "$(date +"%Y-%m-%d %H:%M:%S") REGISTER: ERROR User already exists" >> log.txt
else
  # Add user to users.txt
  echo "$user_name $user_password" >> users.txt
  echo "Successfully registered new user"

  # log it
  echo "$(date +"%Y-%m-%d %H:%M:%S") REGISTER: INFO User $user_name registered successfully" >> log.txt
fi
