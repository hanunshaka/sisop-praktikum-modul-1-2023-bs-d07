#!/bin/bash

# Throw if users directory not exists
if [ ! -d "users" ]
then
  echo "No users have been registered"
  exit 1
else
  cd users
fi

# Get credentials
echo "Enter username:"
read user_name
echo "Enter password:"
read user_password

# Get credentials from users.txt
temp=$(grep "$user_name" users.txt)

# Throw if user not exists
if [ ${#temp} -lt 8 ]
then
  echo "User not found, please register before logging in"
  exit 1
fi

# Tokenize username and user password
username=$(echo $temp | awk '{print $1}')
password=$(echo $temp | awk '{print $2}')

# Throw if password not match and login if password match
if [ "$user_password" != "$password" ]
then
  echo "Password not match"
  echo "$(date +"%Y-%m-%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user $username" >> log.txt
  exit 1    # Indicates error
else
  echo "Login success"
  echo "$(date +"%Y-%m-%d %H:%M:%S") LOGIN: INFO User $username logged in" >> log.txt
  exit 0    # Indicates success
fi
