#!/bin/bash

# mengambil nama file yang akan didekripsi dari hasil enkripsi syslog sebelumnya
read -p "Masukkan nama file yang akan didekripsi: " encrypted_file

# mengambil jam enkripsi dari nama file dengan cara memisahkan nama file yang terletak sebelum tanda '_' pertama setelah itu bagian yang terpisah tadi diambil karakter sebelum tanda ':' pertama
hour=$(echo "$encrypted_file" | cut -d'_' -f1 | cut -d':' -f1)

# membuat variabel nama file dengan mengambil nama file yang dienkripsi sebelumnya dan ditambahkan decrypted sebelumnya
filename="decrypted_$encrypted_file"

# variabel shift yang nantinya akan digunakan untuk mendekripsi file yang diinginkan dengan mengambil variabel hour tadinya yang merupakan jam dienkripsi file syslog dengan mengambil jam dari nama file
shift=$hour

# melakukan while loop yang sama dengan log_encrypt.sh tetapi melakukan dekripsi dari file syslog yang telah dienkripsi menggunakan caesar cipher tadi
while read -n1 c; do
    if [[ $c =~ [a-zA-Z] ]]; then
        if [[ $c =~ [A-Z] ]]; then
            c_ascii=$(printf '%d' "'$c")
            c_ascii_offset=$((c_ascii - 65 - shift + 26))
            c_ascii_offset=$((c_ascii_offset % 26 + 65))
            c=$(printf '%b' $(printf '\\x%x' "$c_ascii_offset"))
        else
            c_ascii=$(printf '%d' "'$c")
            c_ascii_offset=$((c_ascii - 97 - shift + 26))
            c_ascii_offset=$((c_ascii_offset % 26 + 97))
            c=$(printf '%b' $(printf '\\x%x' "$c_ascii_offset"))
        fi
    fi
    printf '%s' "$c"
# menyelesaikan while loop diatas. while loop pada file yang terenkripsi yang diambil dari direktori tempat file hasil log_encrypt.sh 'encrypted file' dan mendekripsi setiap karakter (a-zA-Z) dalam file tersebut, lalu hasil dekripsi akan disimpan kedalam file 'filename' yang letaknya diassign ke tempat yang sama dengan file terenkripsi
done < /tempat/file/backup_syslog/$encrypted_file > /tempat/file/backup_syslog/$filename

