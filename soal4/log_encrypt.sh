#!/bin/bash

# mengambil variabel jam untuk shift alphabet nantinya
time=$(date +'%H')

#membuat format filename sesuai soal 
filename="$(date +'%H:%M_%d:%m:%Y').txt"

# variabel shift nantinya akan digunakan sebagai pergeseran alphabet nantinya sesuai dengan jam backup file syslog dibuat
shift=$time

# membaca setiap karakter dari file syslog dan menyimpannya dalam variabel c secara looping
while read -n1 c; do
    # melakukan enkripsi pada karakter a-z atau A-Z
    if [[ $c =~ [a-zA-Z] ]]; then
        # melakukan enkripsi pada karakter huruf kapital A-Z dengan menggunakan caesar cipher  
        if [[ $c =~ [A-Z] ]]; then
            c_ascii=$(printf '%d' "'$c")
            c_ascii_offset=$((c_ascii - 65 + shift))
            c_ascii_offset=$((c_ascii_offset % 26 + 65))
            # mengubah kode ascii hasil enkirpsi menjadi karakter kembali
            c=$(printf '%b' $(printf '\\x%x' "$c_ascii_offset"))
        # melakukan enkripsi pada karakter huruf kecil a-z dengan caesar cipher dan langkah-langkah yang sama seperti huruf kapital
        else
            c_ascii=$(printf '%d' "'$c")
            c_ascii_offset=$((c_ascii - 97 + shift))
            c_ascii_offset=$((c_ascii_offset % 26 + 97))
            c=$(printf '%b' $(printf '\\x%x' "$c_ascii_offset"))
        fi
    fi
    printf '%s' "$c"
#menyelesaikan loop while. Selama loop berjalan, setiap karakter yang telah dienkripsi dari file syslog akan disimpan ke dalam file dengan nama dan format yang telah ditentukan (variabel filename) menggunakan operator redirect (>). File hasil enkripsi akan disimpan di direktori yang di assign dibawah contoh : /tempat/file/backup syslog/$filename
done < /var/log/syslog > /tempat/file/backup_syslog/$filename

#untuk menjalankan script log_encrypt.sh setiap 2 jam, masukkan line dibawah ke crontab :
#0 */2 * * * /tempat/file/log_encrypt.sh
